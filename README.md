# forecast-server

JSON based REST web API for meteorological and oceanographic (METOC) forecasts.
The server retrieves the forecasts from OPeNDAP servers and slices out
relevant subsets of the data for consumption by the client.

## Installation

### Requirements

Forecast-server is written in Python and has been developed and tested on Linux.
It has not been tested on other platforms.

The following Python packages need to be installed:

* CherryPy: http://cherrypy.org
* ConfigObj: http://www.voidspace.org.uk/python/configobj.html
* NumPy: http://www.numpy.org/
* Pydap: https://github.com/pydap/pydap
* netcdf4-python: https://github.com/Unidata/netcdf4-python

### Ubuntu (16.04 and newer)

```
sudo apt install python3-cherrypy3 python3-configobj python3-numpy python3-netcdf4
sudo pip install Pydap
```

### Clone
```
git clone git@gitlab.com:FCOO/forecast-server.git
```

## Usage

```
cd forecast-server
export PYTHONPATH=${PYTHONPATH}:$(pwd)/lib
cd server
python3 ../scripts/forecast_server.py server_dev.cfg metoc.cfg
```

Check that the server works on:

http://localhost:6050/info/parameters

Substitute "localhost" with the name or IP of your server if you are not running
it on your local machine.

## API documentation

The documentation for the API can be found in the [API documentation](API.md).

