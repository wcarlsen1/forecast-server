FROM ubuntu:latest

MAINTAINER Brian Højen-Sørensen

# Never prompts the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND noninteractive
ENV TERM linux

# Install deb and pip packages
RUN apt-get update \
 && apt-get install -y \
    python3-cherrypy3 \
    python3-configobj \
    python3-netcdf4 \
    python3-pip \
 && pip3 install \
    pydap \
 && apt-get autoremove -y \
 && rm -rf /var/lib/apt/lists/* \
           /root/.cache/* \
           /tmp/* \
           /var/tmp/* \
           /usr/share/man \
           /usr/share/doc \
           /usr/share/doc-base

# Copy stuff from here to target
COPY scripts/forecast_server.py /forecast_server.py
COPY lib /lib
COPY server /server

# Set PYTHONPATH
ENV PYTHONPATH /lib
ENV PYTHONUNBUFFERED 0

# Web service ports
EXPOSE 6050 6060 6070

ENTRYPOINT ["/forecast_server.py", "/server/server_prod.cfg", "/server/metoc.cfg"]
