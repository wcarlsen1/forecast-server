#!/usr/bin/env python3
# encoding: utf-8
"""
Thin web API which provides geolocated access to NetCDF forecasts.
"""
# Standard library imports
import functools
import json
import os
import sys

# External imports
import cherrypy
from configobj import ConfigObj

# Local imports
from metoc import model
from metoc import forecast_exceptions

def read_config(filename, unrepr=True, interpolation=True):
    """Reads in ConfigObj configuration file."""
    # Check if file exists
    if not os.path.exists(filename):
        raise IOError('File not found: %s' % filename)
    # Read in configuration
    config = ConfigObj(filename, unrepr=unrepr, interpolation=interpolation)
    return config

class PrettyFloat(float):
    def __json__(self):
        return '%.8g' % self

def pretty_floats(obj):
    if isinstance(obj, float):
        return float('%.8g' % obj)
    elif isinstance(obj, dict):
        return dict((k, pretty_floats(v)) for k, v in obj.items())
    elif isinstance(obj, (list, tuple)):
        return list(map(pretty_floats, obj))
    return obj

def json_error_handler():
    """\
    This error handler returns a "500 Internal Server Error" for real 
    server errors and 404 "Not Found" in situations where a forecast is not 
    available for a given request. It sets the response to the value of the 
    "args" attribute of the caught exception.
    """
    cherrypy.response.headers['Content-Type'] = "application/json"
    args = sys.exc_info()[1].args
    exc_type = sys.exc_info()[0]
    if exc_type in [forecast_exceptions.NetCDFException]:
        cherrypy.response.status = 404
    elif exc_type in [forecast_exceptions.Invalid]:
        cherrypy.response.status = 400
    else:
        cherrypy.response.status = 500
    if len(args) == 0:
        message = str(sys.exc_info()[1])
    elif len(args) == 1:
        message = args[0]
    else:
        message = str(args)
    result = str.encode(json.dumps({'message': message}))
    cherrypy.response.body = result

def fcoo_jsonify():
    def decorate(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            # Validate input arguments (exclude self)
            cherrypy._cpdispatch.test_callable_spec(func, args[1:], kwargs)
            # Enable CORS
            cherrypy.response.headers['Access-Control-Allow-Origin'] = '*'
            cherrypy.response.headers['Content-Type'] = 'application/json'
            # Return data
            return str.encode(json.dumps(func(*args, **kwargs)))
        return wrapper
    return decorate

class InfoAPI(object):
    def __init__(self, cfgfile):
        cfg = read_config(cfgfile)
        self.params = cfg['Parameters']

    @cherrypy.expose
    @fcoo_jsonify()
    def parameters(self, lon=None, lat=None):
        """\
        Returns parameter names. If position is given as input only return
        parameters with forecasts covering the given position.
        """
        # Validate and convert lat and lon
        if lat is not None:
            try:
                lat = float(lat)
            except:
                msg = "Invalid 'lat' parameter"
                raise forecast_exceptions.Invalid(msg)
        if lon is not None:
            try:
                lon = float(lon)
            except:
                msg = "Invalid 'lon' parameter"
                raise forecast_exceptions.Invalid(msg)

        params = self.params.keys()

        # Only include parameters covering input point
        if lon is not None and lat is not None:
            outparams = []
            for par in params:
                for fmodel in self.params[par]:
                    lon0, lat0, lon1, lat1 = self.params[par][fmodel]['area']
                    if lon0 <= lon <= lon1 and lat0 <= lat <= lat1:
                        outparams.append(par)
                        break
            params = outparams

        # Return data
        return params

    @cherrypy.expose
    @fcoo_jsonify()
    def area(self, variable):
        """Returns the domain limits for the input variable."""
        # Validate variable
        if variable is None:
            msg = 'Please specify variable to extract'
            raise forecast_exceptions.Invalid(msg)
        if variable not in self.params:
            msg = 'Variable %s not found' % (variable)
            raise forecast_exceptions.Invalid(msg)

        xmin = 999999.9
        ymin = 999999.9
        xmax = -999999.9
        ymax = -999999.9
        for fmodel in self.params[variable]:
            area = self.params[variable][fmodel]['area']
            x0, y0, x1, y1 = area
            xmin = min(xmin, x0)
            xmax = max(xmax, x1)
            ymin = min(ymin, y0)
            ymax = max(ymax, y1)
        outarea = {
            'lon_min': xmin,
            'lon_max': xmax,
            'lat_min': ymin,
            'lat_max': ymax
        }

        # Return data
        return outarea

    @cherrypy.expose
    @fcoo_jsonify()
    def timesteps(self, variable, lon=None, lat=None):
        """\
        Returns the timesteps for the input variable at a specific 
        position.
        """
        # Input validation
        if variable not in self.params:
            msg = 'Variable %s not found' % (variable)
            raise forecast_exceptions.Invalid(msg)

        # Request timesteps
        fmodels = self.params[variable]
        for modelName in fmodels:
            fmodel = fmodels[modelName]
            files = fmodel['files']
            params = fmodel['ncvars']
            try:
                dataset = model.Dataset(files, params)
            except:
                msg = 'Failed opening %s for parameters %s' % \
                      (files, params)
                raise RuntimeError(msg)

            try:
                t = dataset.timesteps(params)
            except:
                msg = 'Failed getting time series from %s for parameters %s' % \
                      (files, parameters)
                raise RuntimeError(msg)

        # Return data
        return t


class DataAPI(object):
    def __init__(self, cfgfile):
        cfg = read_config(cfgfile)
        self.parameters = cfg['Parameters']

    @cherrypy.expose
    @fcoo_jsonify()
    def timeseries(self, variables, lat, lon):
        """Returns timeseries data."""
        # Input validation
        # Validate variables
        if variables is None:
            msg = 'Please specify variables to extract'
            raise forecast_exceptions.Invalid(msg)
        variables = variables.split(',')
        for variable in variables:
            if variable not in self.parameters:
                msg = 'Variable %s not found' % (variable)
                raise forecast_exceptions.Invalid(msg)

        # Validate and convert lat and lon
        try:
            lat = float(lat)
        except:
            msg = "Invalid 'lat' parameter"
            raise forecast_exceptions.Invalid(msg)
        try:
            lon = float(lon)
        except:
            msg = "Invalid 'lon' parameter"
            raise forecast_exceptions.Invalid(msg)

        outdata = {}
        for variable in variables:
            # Request timeseries
            fmodels = self.parameters[variable]
            for modelName in fmodels:
                fmodel = fmodels[modelName]
                files = fmodel['files']
                params = fmodel['ncvars']
                lon0, lat0, lon1, lat1 = fmodel['area']
                if lat > lat1 or lat < lat0 or lon > lon1 or lon < lon0:
                    # Skip this model since out of domain
                    continue
                try:
                    dataset = model.Dataset(files, params)
                except:
                    msg = 'Failed opening %s for parameters %s' % \
                          (files, params)
                    raise RuntimeError(msg)
                try:
                    data = dataset.timeseries(lat, lon)
                    outdata[variable] = data
                except:
                    msg = 'Failed getting time series from %s for parameters %s' % \
                          (files, params)
                    raise RuntimeError(msg)

        # If no data
        if len(outdata.keys()) == 0:
            msg = 'No forecast at requested position'
            raise forecast_exceptions.NetCDFException(msg)

        # Return data
        return pretty_floats(outdata)

    @cherrypy.expose
    @fcoo_jsonify()
    def profile(self, variables, time, lat, lon):
        """Returns timeseries data."""
        # Input validation
        # Validate variables
        if variables is None:
            msg = 'Please specify variables to extract'
            raise forecast_exceptions.Invalid(msg)
        variables = variables.split(',')
        for variable in variables:
            if variable not in self.parameters:
                msg = 'Variable %s not found' % (variable)
                raise forecast_exceptions.Invalid(msg)

        # Validate and convert lat and lon
        try:
            lat = float(lat)
        except:
            msg = "Invalid 'lat' parameter"
            raise forecast_exceptions.Invalid(msg)
        try:
            lon = float(lon)
        except:
            msg = "Invalid 'lon' parameter"
            raise forecast_exceptions.Invalid(msg)

        # TODO: Validate time

        outdata = {}
        for variable in variables:
            # Request profile
            fmodels = self.parameters[variable]
            for modelName in fmodels:
                fmodel = fmodels[modelName]
                files = fmodel['files']
                params = fmodel['ncvars']
                lon0, lat0, lon1, lat1 = fmodel['area']
                if lat > lat1 or lat < lat0 or lon > lon1 or lon < lon0:
                    # Skip this model since out of domain
                    continue
                try:
                    dataset = model.Dataset(files, params)
                except:
                    msg = 'Failed opening %s for parameters %s' % \
                          (files, params)
                    raise RuntimeError(msg)
                try:
                    data = dataset.profile(lat, lon)
                    outdata[variable] = data
                except:
                    msg = 'Failed getting profile from %s for parameters %s' % \
                          (files, params)
                    raise RuntimeError(msg)

        # If no data
        if len(outdata.keys()) == 0:
            msg = 'No forecast at requested position and time'
            raise forecast_exceptions.NetCDFException(msg)

        # Return data
        return pretty_floats(outdata)

class Forecast(object):
    # We use a custom error handler for the Web API
    _cp_config = {'request.error_response': json_error_handler}

    @cherrypy.expose
    @fcoo_jsonify()
    def status(self, *args, **kwargs):
        """Returns server status."""
        msg = {'message': 'Server is UP'}
        return msg

    def __init__(self, cfgfile):
        self.info = InfoAPI(cfgfile)
        self.data = DataAPI(cfgfile)

def main():
    server_cfgfile = sys.argv[1]
    metoc_cfgfile = sys.argv[2]
    cherrypy.quickstart(Forecast(metoc_cfgfile), '/', server_cfgfile)

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()', 'forecast.prof')
    exit(main())
